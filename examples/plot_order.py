import numpy as np
import matplotlib.pyplot as plt
from matplotlib import patches 
import itertools as it
from math import sqrt
import sys

def extract_order(GRho, shape, T = False):
    nsites = np.product(shape)
    rhoA, rhoB, kappaBA = GRho[:nsites,:nsites], np.eye(nsites) - GRho[nsites:,nsites:], \
            GRho[nsites:, :nsites]
    cA, cB = np.diag(rhoA), np.diag(rhoB)
    denh = np.asarray(1. - cA - cB).reshape(shape)
    dens = np.asarray(0.5 * (cA - cB)).reshape(shape)
    if T:
        denh, dens = denh.T, dens.T

    kappa = (kappaBA + kappaBA.T) / sqrt(2)
    sites = list(it.product(*map(range, shape)))
    pair_map = {}
    for i, (x,y) in enumerate(sites):
        if (x+1,y) in sites:
            j = sites.index((x+1,y))
            if T:
                pair_map[(y,x), (y, x+1)] = kappa[i,j]
            else:
                pair_map[(x,y), (x+1,y)] = kappa[i,j]
        if (x,y+1) in sites:
            j = sites.index((x,y+1))
            if T:
                pair_map[(y,x), (y+1,x)] = kappa[i,j]
            else:
                pair_map[(x,y), (x,y+1)] = kappa[i,j]
    p = [pair_map[key] for key in pair_map]
    return denh, dens, pair_map

def plot_order(GRho, shape, transpose = False):
    denh, dens, pair_map = extract_order(GRho, shape, transpose)
    if transpose:
        shape = shape[::-1]
    fig = plt.figure(figsize = shape)
    plt.xticks(visible = False)
    plt.yticks(visible = False)
    plt.xlim([0, shape[0]])
    plt.ylim([0, shape[1]])
    ax = fig.gca()
    for p1, p2 in pair_map:
        val = pair_map[p1, p2]
        color = "g" if val > 0 else "b"
        breadth = abs(val * 10)
        x1, y1 = p1
        x2, y2 = p2
        if x1 == x2:
            ax.add_patch(patches.Rectangle((x1+0.5-breadth*0.5, y1+0.5), breadth, y2-y1, color = color, alpha = 0.5))
        elif y1 == y2:
            ax.add_patch(patches.Rectangle((x1+0.5, y1+0.5-breadth*0.5), x2-x1, breadth, color = color, alpha = 0.5))
        else:
            raise Exception("Not nearest neighbor pairing")
    for (x, y) in it.product(*map(range, shape)):
        cir = plt.Circle((x+0.5, y+0.5), denh[x,y] * 1.3, color = 'r', ec = 'k', alpha = 0.9)
        ax.add_artist(cir)
        if abs(dens[x,y]) > 5e-3:
            arr = plt.arrow(x+0.5, y+0.5-dens[x,y], 0, dens[x,y]*2, head_width = 0.03, \
                    head_length = 0.02, linewidth = 2, color = 'k')
        else:
            arr = plt.Circle((x+0.5, y+0.5), 0.01, color = 'k')
            ax.add_artist(arr)
    
    if shape[1] == 2:
        for x in range(shape[0]):
            plt.text(x+0.28, 2.15, "%.2f" % (np.sum(denh[x])/2), fontsize = 15, color = 'r')
            plt.text(x+0.28, -0.33, "%.2f" % (abs(dens[x,0]-dens[x,1])/2), fontsize = 15, color = 'k')

if __name__ == "__main__":
    plot_order(np.load(sys.argv[1]), [int(sys.argv[2]), int(sys.argv[3])], transpose = False)
    plt.savefig(sys.argv[1].replace("npy", "png"), dpi = 200, bbox_inches = "tight")
