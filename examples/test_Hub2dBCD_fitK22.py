import libdmet.utils.logger as log
import libdmet.dmet.HubbardBCS as dmet
import numpy as np
import numpy.linalg as la
import os

log.verbose = "DEBUG0"

U = 4.
LatSize = [128, 128]
ImpSize = [2, 2]
Filling = 0.875/2
MaxIter = 30
M = 600
DiisStart = 4
TraceStart = 2
DiisDim = 4

Lat = dmet.SquareLattice(*(LatSize + ImpSize))
Ham = dmet.Ham(Lat, U)
Lat.setHam(Ham)

vcor = dmet.VcorLocal(False, True, Lat.supercell.nsites)

fvcor = "data/vcor_2dbcs_fitK22.npy"

if os.path.exists(fvcor):
    log.result("Read vcor from disk")
    vcor.update(np.load(fvcor))
    DiisStart = 1
else:
    log.result("Antiferromagnetic initial guess of vcor")
    vcor = dmet.AFInitGuess(ImpSize, U, Filling, rand = 0.001)

Mu = U * Filling
dc = dmet.FDiisContext(DiisDim) # I don't know yet whether diis needs to be changed


conv = False

history = dmet.IterHistory()

solver = dmet.impurity_solver.StackBlock(nproc = 1, nthread = 1, \
        nnode = 1, bcs = True, reorder = False, tol = 1e-7, maxM = M,
		TmpDir = "/home/sunchong/scratch/dmet", SharedDir = "/home/sunchong/scratch/gpfs/dmet")

log.section("\nfitting chemical potential\n")
_, Mu = dmet.HartreeFockBogoliubov(Lat, vcor, Filling, Mu)

for iter in range(MaxIter):
    log.section("\nDMET Iteration %d\n", iter)

    log.section ("\nsolving mean-field problem\n")
    log.result("Vcor =\n%s", vcor.get())
    log.result("Mu (guess) = %20.12f", Mu)
    GRho, Mu = dmet.HartreeFockBogoliubov(Lat, vcor, None, Mu)

    log.section("\nconstructing impurity problem\n")
    ImpHam, H_energy, basis = dmet.ConstructImpHam(Lat, GRho, vcor, Mu)

    log.section("\nsolving impurity problem\n")
    GRhoEmb, EnergyEmb, ImpHam, dmu = \
            dmet.SolveImpHam_with_fitting(Lat, Filling, ImpHam, basis, solver)
    Mu += dmu
    vcor = dmet.addDiag(vcor, dmu)
    GRhoImp, EnergyImp, nelecImp = \
            dmet.transformResults(GRhoEmb, EnergyEmb, Lat, basis, ImpHam, H_energy, dmu)

    np.save("data/GRhoImp_K22.npy", GRhoImp)
    log.section("\n Impurity Energy: %20.20f\n", EnergyImp)

    log.section("\nfitting correlation potential\n")
#kinetic fitting
    vcor_new, err = dmet.FitVcor(GRho = GRhoEmb, lattice = Lat, \
                        basis = basis, vcor = vcor, mu = Mu, \
            MaxIter1 = 0, MaxIter2 = 1, kinetic = True)

#fragment energy fitting
#   vcor_new, err = dmet.FitVcor(GRho = GRhoEmb, lattice = Lat, \
#           basis = basis, vcor = vcor, mu = Mu, E = EnergyEmb, \
#           ImpHam = ImpHam, H_energy = H_energy, dmu = dmu, \
#           MaxIter1 = 0, MaxIter2 = 1, Efrag = True)

# the following block can be commented
    if iter >= TraceStart:
        # to avoid spiral increase of vcor and mu
        log.result("Keep trace of vcor unchanged")
        ddiagV = np.average(np.diagonal(\
                (vcor_new.get()-vcor.get())[:2], 0, 1, 2))
        vcor_new = dmet.addDiag(vcor_new, -ddiagV)

    log.section("\nfitting chemical potential\n")
    GRho, Mu_new = dmet.HartreeFockBogoliubov(Lat, vcor_new, Filling, Mu)
    log.result("dMu = %20.12f", Mu_new - Mu)

    history.update(EnergyImp, err, nelecImp, \
            np.max(abs(vcor.get() - vcor_new.get())), dc)

    if np.max(abs(vcor.get() - vcor_new.get())) < 1e-7:
        conv = True
        break

    if not conv:
        skipDiis = (iter < DiisStart) and (la.norm(vcor_new.param - vcor.param) > 0.01)
        pvcor, dpvcor, _ = dc.Apply( \
                np.hstack((vcor_new.param, Mu_new)), \
                np.hstack((vcor_new.param - vcor.param, Mu_new - Mu)), \
                Skip = skipDiis)
        vcor.update(pvcor[:-1])
        np.save(fvcor, vcor.param)
        Mu = pvcor[-1]

solver.cleanup()

if conv:
    log.result("DMET converged")
else:
    log.result("DMET cannot converge")
