from pyscf import fci as pyfci
from pyscf.ft import ftfci
import libdmet.utils.logger as log
import numpy as np
from copy import deepcopy


class ftfci_solver(object):
    def __init__(self, T=0, norb=None, nelec=None, civector=None, restricted=False,\
            M=50, nsmpl=2000, bogoliubov=False):
        log.info("Using finite temperature pyscf fci as the impurity solver")
        log.eassert(bogoliubov==False, "Particle symmetry breaking version is not implemented!\n")
        self.T = T
        self.norb = norb
        self.nelec = nelec
        self.civector = civector
        self.restricted = restricted
        self.dim_krylov = M
        self.nsmpl = nsmpl
        self.hdiag = None
        self.h1e = None
        self.h2e = None

    def kernel(self, Ham):
        norb = Ham.norb
        self.norb = norb
        nelec = self.nelec
        M = self.dim_krylov
        nsmpl = self.nsmpl
        T = self.T
        if nelec == None:
            nelec = norb
            self.nelec = nelec
        h1e = Ham.H1['cd']
        h2e = Ham.H2['ccdd']
        if self.restricted:
            log.warning("FCI for restricted spin is not implemented yet\n!")
            log.warning("setting restricted to False!")
            self.restricted = False
        else:
            h2e[[1,2],:] = h2e[[2,1],:] #the order pyscf stored h2e is different

        self.h1e = deepcopy(h1e)
        self.h2e = deepcopy(h2e)
        if T < 1e-3:
            Eemb, civec = pyfci.direct_uhf.kernel(h1e, h2e, norb, nelec)
            self.civector=civec
        else:
            Eemb = ftfci.kernel_ft_smpl(h1e, h2e, norb, nelec, T, \
                m=M, nsmpl=nsmpl, uhf=True) 
        return Eemb

    def fci_rdm1s(self):
        T = self.T
        norb = self.norb
        nelec = self.nelec
        hdiag = deepcopy(self.hdiag)
        M = self.dim_krylov
        civec = self.civector
        nsmpl = self.nsmpl
        h1e = self.h1e
        h2e = self.h2e
        if T < 1e-3:
            log.eassert(self.civector is not None, "The fci_solver needs to be called first to initialize the solver!\n")
            rdm1a, rdm1b = pyfci.direct_spin1.make_rdm1s(civec, norb, nelec) #tuple : alpha, beta
        else:
            rdm1a, rdm1b = ftfci.rdm1s_ft_smpl(h1e, h2e, norb, \
                nelec, T, m=M, nsmpl=nsmpl, uhf=True)

        log.section("Embbeding 1-particle density matrices (alpha, beta):\n")
        log.section("%s\n%s\n"%(rdm1a, rdm1b))
        return rdm1a, rdm1b
        
    def fci_rdm12s(self):
        T = self.T
        norb = self.norb
        nelec = self.nelec
        hdiag = deepcopy(self.hdiag)
        civec = self.civector
        M = self.dim_krylov
        nsmpl = self.nsmpl
        h1e = self.h1e
        h2e = self.h2e
        if T < 1e-3:
            log.eassert(self.civector is not None, "The fci_solver needs to be called first to initialize the solver!\n")
            (dm1a, dm1b), (dm2aa, dm2ab, dm2ba, dm2bb) = pyfci.direct_spin1.trans_rdm1s(civec, civec, norb, nelec)
        else:
            (dm1a, dm1b), (dm2aa, dm2ab, dm2ba, dm2bb) = ftfci.rdm12s_ft_smpl(h1e, h2e, norb, nelec, T, m=M, nsmpl=nsmpl, uhf=True)
            
        return np.asarray([dm1a, dm1b]), np.asarray([dm2aa, dm2bb, dm2ab]) # the order changed
    
    def fci_rdm2s(self):
        return self.rdm12s()[1]

    def run(self, Ham, nelec=None, norb=None, restricted=False):
        Eimp = self.kernel(Ham)
        rdm1a, rdm1b = self.fci_rdm1s()
        return Eimp, np.asarray([rdm1a, rdm1b])

    def cleanup(self):
        pass


if __name__ == "__main__":
    fci=fci_solver()
