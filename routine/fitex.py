import numpy as np
import numpy.linalg as la
from copy import deepcopy
from math import sqrt
import itertools as it
import libdmet.utils.logger as log
from libdmet.system import integral
from bcs_helper import *
from slater import MatSqrt, orthonormalizeBasis
from mfd import assignocc, HFB
from fit import minimize
from libdmet.utils.misc import mdot, find
from libdmet import settings

def FitVcorFullEx13(GRho, lattice, basis, vcor, mu,  \
            dmet, solver, filling, MaxIter=1, Fullgrad=True, **kwargs):
    nscsites = lattice.supercell.nsites

    log.info("using the impurity fitting - Version 13 by ChongSun\n")
    if Fullgrad:
        log.info("Full Gradient will be calculated!\n")
    else:
        log.info("Will use approximated Gradient!\n")
    
    class chempot(object):
        def __init__(self, mu):
            self.val = mu
        def update(self, mu):
            self.val = mu
        def addmu(self, dmu):
            self.val += dmu

    # define a class for mu to update it
    cMu = chempot(mu)

    def impsolver(param, mu, fitmu=False):
        vcor_tmp = deepcopy(vcor)
        vcor_tmp.update(param) 
        # HFB will not influence mu and vcor
        if fitmu:
            log.debug(0, "Solving the impurity problem - mu will be fitted!")
            GRho, mu = dmet.HartreeFockBogoliubov(lattice, vcor_tmp, filling, mu)
            ImpHam, H_energy, basis = dmet.ConstructImpHam(lattice, GRho, vcor_tmp, mu)

            GRhoEmb, EnergyEmb, ImpHam_n, dmu = \
            dmet.SolveImpHam_with_fitting(lattice, filling, ImpHam, basis, solver)
        
    #   log.section("The Correlation potential after fitting is \n%s", vcor_tmp.param) 

            _, Efrag, _ = transformResults(GRhoEmb, EnergyEmb, lattice, basis, ImpHam_n, H_energy, dmu)
        
            mu += dmu
            vcor_tmp = dmet.addDiag(vcor_tmp, dmu)
        else:
            log.debug(0, "solving the impurity problem - mu will not be fitted")
            GRho, mu = dmet.HartreeFockBogoliubov(lattice, vcor_tmp, None, mu)
            ImpHam, H_energy, basis = dmet.ConstructImpHam(lattice, GRho, vcor_tmp, mu)

            GRhoEmb, Efrag = solver.run(ImpHam)
        return Efrag, GRhoEmb, mu, vcor_tmp.param



    def costfunc(param, v=False, updt=False, 
            GRhoEmb=None, Efrag=None, fullgrad=True):

        # calculating the impurity energy
#        log.section("\n before updt, mu = %f\n"%cMu.val)
        if fullgrad:
            if updt:
                Efrag, GRhoEmb, mu, param_tmp = impsolver(param, cMu.val, fitmu=True)
                log.info("\nupdating vcor and mu\n")
                cMu.update(mu)
                param = param_tmp  # param is changed here
            else:
                Efrag, GRhoEmb, _, _ = impsolver(param, cMu.val, fitmu=False)
        else:
            if GRhoEmb==None or Efrag==None: # if fullgrad==False, GRhoEmb and Efrag must be provided!
                Efrag, GRhoEmb, _, _ = impsolver(param, cMu.val)
      
        mu = cMu.val
#        log.section("\n updt = %r ; mu = %f\n"%(updt, mu))
        vcor_tmp = deepcopy(vcor)
        vcor_tmp.update(param)
        verbose = log.verbose
        log.verbose = "RESULT"
        # calculate rho_mf
        GRhoT,_,_ = HFB(lattice, vcor_tmp, False, mu, beta = np.inf)
        log.verbose = verbose

        tempRdm = map(extractRdm, GRhoT)
        rhoAT = np.asarray([rhoA for (rhoA, rhoB, kappaBA) in tempRdm])
        rhoBT = np.asarray([rhoB for (rhoA, rhoB, kappaBA) in tempRdm])
        kappaBA0 = tempRdm[0][2]

        # rho_imp
        rhoA, rhoB, kappaBA = extractRdm(GRhoEmb)
        rhoAImp = rhoA[:nscsites, :nscsites]
        rhoBImp = rhoB[:nscsites, :nscsites]
        kappaBAImp = kappaBA[:nscsites, :nscsites]

        # calculating the constraint

        constraint = np.sum((vcor_tmp.get()[0]-mu*np.eye(nscsites)) * (rhoAT[0] - rhoAImp)) + \
                np.sum((vcor_tmp.get()[1]-mu*np.eye(nscsites)) * (rhoBT[0] - rhoBImp)) + \
                np.sum(vcor_tmp.get()[2] * (kappaBA0 - kappaBAImp).T) + \
                np.sum(vcor_tmp.get()[2].T * (kappaBA0 - kappaBAImp))
        if v:
            return Efrag, constraint
        elif updt:
            return -(Efrag + constraint), param 
        else:
            return -(Efrag + constraint)
        
    def costfunc_updt(param):
        return costfunc(param, updt = True, fullgrad = True)[0]
    def costfunc_tuple(param):
        return costfunc(param, updt = True, fullgrad = True)
    
    def numgrad(param, dx = 1e-4):
        log.info("\n Calculating gradient\n")
        if Fullgrad==False:
            _Efrag, _GRhoEmb, _, _ = impsolver(param, cMu.val)
        else:
            _Efrag, _GRhoEmb = None, None
        res = np.empty_like(param)
        for iter in range(len(param)):
            lparam, rparam = param.copy(), param.copy()
            lparam[iter] -= dx
            rparam[iter] += dx
            res[iter] = (costfunc(rparam, updt = False, Efrag=_Efrag, GRhoEmb=_GRhoEmb, fullgrad=Fullgrad) - costfunc(lparam, updt = False, Efrag=_Efrag, GRhoEmb=_GRhoEmb, fullgrad=Fullgrad))/(2.*dx)
            log.debug(0, "The costfunction difference is %f"%res[iter])
        log.section("\nGradient\n%s", res)
        res_norm = la.norm(res)
        log.section("\nGradNorm:   %10.10f"%res_norm)
        return res

    from scipy_optimize import fmin_bfgs
    ex_begin, c_begin = costfunc(vcor.param, v = True, updt = False)
    log.info("begin: \nimpurity energy = %20.20f     constraint = %20.12f", ex_begin, c_begin)           
    param0 = deepcopy(vcor.param)
    param = fmin_bfgs(costfunc_updt, param0, f2=costfunc_tuple, \
            fprime=numgrad, gtol=1e-5, disp=True, maxiter=100)
    ex_end, c_end = costfunc(param, v = True, updt = False)
    log.info("end: \nimpurity energy = %20.12f    constraint = %20.12f", ex_end, c_end)
    
    vcor.update(param)
    return vcor, c_begin, c_end


def FitVcorFullEx14(GRho, lattice, basis, vcor, mu,  \
            dmet, solver, filling, MaxIter=1, Fullgrad=True, **kwargs):
    nscsites = lattice.supercell.nsites

    log.info("using the impurity fitting - Version 14 by ChongSun\n")
    if Fullgrad:
        log.info("Full Gradient will be calculated!\n")
    else:
        log.info("Will use approximated Gradient!\n")

    class chempot(object):
        def __init__(self, mu):
            self.val = mu
        def update(self, mu):
            self.val = mu
        def addmu(self, dmu):
            self.val += dmu

    # define a class for mu to update it
    cMu = chempot(mu)

    def impsolver(param, mu, fitmu=False):
        vcor_tmp = deepcopy(vcor)
        vcor_tmp.update(param) 
        # HFB will not influence mu and vcor
        if fitmu:
            log.debug(0, "Solving the impurity problem - mu will be fitted!")
            GRho, mu = dmet.HartreeFockBogoliubov(lattice, vcor_tmp, filling, mu)
            ImpHam, H_energy, basis = dmet.ConstructImpHam(lattice, GRho, vcor_tmp, mu)

            GRhoEmb, EnergyEmb, ImpHam_n, dmu = \
            dmet.SolveImpHam_with_fitting(lattice, filling, ImpHam, basis, solver)
        
    #   log.section("The Correlation potential after fitting is \n%s", vcor_tmp.param) 

            _, Efrag, _ = transformResults(GRhoEmb, EnergyEmb, lattice, basis, ImpHam_n, H_energy, dmu)
        
            mu += dmu
            vcor_tmp = dmet.addDiag(vcor_tmp, dmu)
        else:
            log.debug(0, "solving the impurity problem - mu will not be fitted")
            GRho, mu = dmet.HartreeFockBogoliubov(lattice, vcor_tmp, None, mu)
            ImpHam, H_energy, basis = dmet.ConstructImpHam(lattice, GRho, vcor_tmp, mu)

            GRhoEmb, Efrag = solver.run(ImpHam)
        return Efrag, GRhoEmb, mu, vcor_tmp.param


    _Efrag, _GRhoEmb, _, _ = impsolver(vcor.param, cMu.val)

    def costfunc(param, v=False, updt=False, 
            GRhoEmb=_GRhoEmb, Efrag=_Efrag, fullgrad=True):

        # calculating the impurity energy
#        log.section("\n before updt, mu = %f\n"%cMu.val)
        if fullgrad:
            if updt:
                Efrag, GRhoEmb, mu, param_tmp = impsolver(param, cMu.val, fitmu=True)
            else:
                Efrag, GRhoEmb, _, _ = impsolver(param, cMu.val, fitmu=False)
            
        mu = cMu.val
#        log.section("\n updt = %r ; mu = %f\n"%(updt, mu))
        vcor_tmp = deepcopy(vcor)
        vcor_tmp.update(param)
        verbose = log.verbose
        log.verbose = "RESULT"
        # calculate rho_mf
        GRhoT,_,_ = HFB(lattice, vcor_tmp, False, mu, beta = np.inf)
        log.verbose = verbose

        tempRdm = map(extractRdm, GRhoT)
        rhoAT = np.asarray([rhoA for (rhoA, rhoB, kappaBA) in tempRdm])
        rhoBT = np.asarray([rhoB for (rhoA, rhoB, kappaBA) in tempRdm])
        kappaBA0 = tempRdm[0][2]

        # rho_imp
        rhoA, rhoB, kappaBA = extractRdm(GRhoEmb)
        rhoAImp = rhoA[:nscsites, :nscsites]
        rhoBImp = rhoB[:nscsites, :nscsites]
        kappaBAImp = kappaBA[:nscsites, :nscsites]

        # calculating the constraint

        constraint = np.sum((vcor_tmp.get()[0]-mu*np.eye(nscsites)) * (rhoAT[0] - rhoAImp)) + \
                np.sum((vcor_tmp.get()[1]-mu*np.eye(nscsites)) * (rhoBT[0] - rhoBImp)) + \
                np.sum(vcor_tmp.get()[2] * (kappaBA0 - kappaBAImp).T) + \
                np.sum(vcor_tmp.get()[2].T * (kappaBA0 - kappaBAImp))
        if v:
            return Efrag, constraint
        elif updt:
            return -(Efrag + constraint), param 
        else:
            return -(Efrag + constraint)
        
    def costfunc_updt(param):
        return costfunc(param, updt = True, fullgrad = True)[0]
    def costfunc_tuple(param):
        return costfunc(param, updt = True, fullgrad = True)
    
    def numgrad(param, dx = 1e-4):
        log.info("\n Calculating gradient\n")
        if Fullgrad==False:
            _Efrag, _GRhoEmb, _, _ = impsolver(param, cMu.val)
        else:
            _Efrag, _GRhoEmb = None, None
        res = np.empty_like(param)
        for iter in range(len(param)):
            lparam, rparam = param.copy(), param.copy()
            lparam[iter] -= dx
            rparam[iter] += dx
            res[iter] = (costfunc(rparam, updt = False, Efrag=_Efrag, GRhoEmb=_GRhoEmb, fullgrad=Fullgrad) - costfunc(lparam, updt = False, Efrag=_Efrag, GRhoEmb=_GRhoEmb, fullgrad=Fullgrad))/(2.*dx)
        log.section("\nGradient\n%s", res)
        res_norm = la.norm(res)
        log.section("\nGradNorm:   %10.10f"%res_norm)

        return res

#    from scipy_optimize import _minimize_bfgs as minimize_bfgs
    from scipy_optimize import fmin_bfgs
    ex_begin, c_begin = costfunc(vcor.param, v = True, updt = False)
    log.info("begin: \nimpurity energy = %20.20f     constraint = %20.12f", ex_begin, c_begin)           
    param0 = deepcopy(vcor.param)
#    param = minimize_bfgs(costfunc_updt, param0, jac=numgrad, disp=True).x
    param = fmin_bfgs(costfunc_updt, param0, f2=costfunc_tuple, \
            fprime=numgrad, gtol=1e-5, disp=True, maxiter=100)
    ex_end, c_end = costfunc(param, v = True, updt = False)
    log.info("end: \nimpurity energy = %20.12f    constraint = %20.12f", ex_end, c_end)
    
    vcor.update(param)
    return vcor, c_begin, c_end


def FitVcorFullEx15(GRho, lattice, basis, vcor, mu,  \
            dmet, solver, filling, MaxIter=1, Fullgrad=True, **kwargs):
    nscsites = lattice.supercell.nsites

    log.info("using the impurity fitting - Version 15\n")
    
    class chempot(object):
        def __init__(self, mu):
            self.val = mu
        def update(self, mu):
            self.val = mu
        def addmu(self, dmu):
            self.val += dmu

    # define a class for mu to update it
    cMu = chempot(mu)

    def impsolver(param, mu, fitmu=False):
        vcor_tmp = deepcopy(vcor)
        vcor_tmp.update(param) 
        # HFB will not influence mu and vcor
        if fitmu:
            log.debug(0, "Solving the impurity problem - mu will be fitted!")
            GRho, mu = dmet.HartreeFockBogoliubov(lattice, vcor_tmp, filling, mu)
            ImpHam, H_energy, basis = dmet.ConstructImpHam(lattice, GRho, vcor_tmp, mu)

            GRhoEmb, EnergyEmb, ImpHam_n, dmu = \
            dmet.SolveImpHam_with_fitting(lattice, filling, ImpHam, basis, solver)
        
    #   log.section("The Correlation potential after fitting is \n%s", vcor_tmp.param) 

            _, Efrag, _ = transformResults(GRhoEmb, EnergyEmb, lattice, basis, ImpHam_n, H_energy, dmu)
        
            mu += dmu
            vcor_tmp = dmet.addDiag(vcor_tmp, dmu)
        else:
            log.debug(0, "solving the impurity problem - mu will not be fitted")
            GRho, mu = dmet.HartreeFockBogoliubov(lattice, vcor_tmp, None, mu)
            ImpHam, H_energy, basis = dmet.ConstructImpHam(lattice, GRho, vcor_tmp, mu)

            GRhoEmb, Efrag = solver.run(ImpHam)
        return Efrag, GRhoEmb, mu, vcor_tmp.param


    _Efrag, _GRhoEmb, _, _ = impsolver(vcor.param, cMu.val)

    def costfunc(param, v=False, updt=False, 
            GRhoEmb=_GRhoEmb, Efrag=_Efrag, fullgrad=True):

        # calculating the impurity energy
#        log.section("\n before updt, mu = %f\n"%cMu.val)
        if fullgrad:
            if updt:
                Efrag, GRhoEmb, mu, param_tmp = impsolver(param, cMu.val, fitmu=True)
                log.info("\nupdating vcor and mu\n")
            else:
                Efrag, GRhoEmb, _, _ = impsolver(param, cMu.val, fitmu=False)
            
        mu = cMu.val
#        log.section("\n updt = %r ; mu = %f\n"%(updt, mu))
        vcor_tmp = deepcopy(vcor)
        vcor_tmp.update(param)
        verbose = log.verbose
        log.verbose = "RESULT"
        # calculate rho_mf
        GRhoT,_,_ = HFB(lattice, vcor_tmp, False, mu, beta = np.inf)
        log.verbose = verbose

        tempRdm = map(extractRdm, GRhoT)
        rhoAT = np.asarray([rhoA for (rhoA, rhoB, kappaBA) in tempRdm])
        rhoBT = np.asarray([rhoB for (rhoA, rhoB, kappaBA) in tempRdm])
        kappaBA0 = tempRdm[0][2]

        # rho_imp
        rhoA, rhoB, kappaBA = extractRdm(GRhoEmb)
        rhoAImp = rhoA[:nscsites, :nscsites]
        rhoBImp = rhoB[:nscsites, :nscsites]
        kappaBAImp = kappaBA[:nscsites, :nscsites]

        # calculating the constraint

        constraint = np.sum((vcor_tmp.get()[0]-mu*np.eye(nscsites)) * (rhoAT[0] - rhoAImp)) + \
                np.sum((vcor_tmp.get()[1]-mu*np.eye(nscsites)) * (rhoBT[0] - rhoBImp)) + \
                np.sum(vcor_tmp.get()[2] * (kappaBA0 - kappaBAImp).T) + \
                np.sum(vcor_tmp.get()[2].T * (kappaBA0 - kappaBAImp))
        if v:
            return Efrag, constraint
        elif updt:
            return -(Efrag + constraint), param 
        else:
            return -(Efrag + constraint)
        
    def costfunc_updt(param):
        return costfunc(param, updt = True, fullgrad = True)[0]
    def costfunc_tuple(param):
        return costfunc(param, updt = True, fullgrad = True)
    
    def numgrad(param, dx = 1e-4):
        log.info("\n Calculating gradient\n")
        if Fullgrad==False:
            _Efrag, _GRhoEmb, _, _ = impsolver(param, cMu.val)
        else:
            _Efrag, _GRhoEmb = None, None
        res = np.empty_like(param)
        for iter in range(len(param)):
            lparam, rparam = param.copy(), param.copy()
            lparam[iter] -= dx
            rparam[iter] += dx
            res[iter] = (costfunc(rparam, updt = False, Efrag=_Efrag, GRhoEmb=_GRhoEmb, fullgrad=Fullgrad) - costfunc(lparam, updt = False, Efrag=_Efrag, GRhoEmb=_GRhoEmb, fullgrad=Fullgrad))/(2.*dx)
        log.section("\nGradient\n%s", res)
        return res


    from scipy_optimize import _minimize_bfgs as minimize_bfgs
    from scipy_optimize import _minimize_neldermead as minimize_nm
    ex_begin, c_begin = costfunc(vcor.param, v = True, updt = False)
    log.info("begin: \nimpurity energy = %20.20f     constraint = %20.12f", ex_begin, c_begin)           
    param = minimize_bfgs(costfunc_updt, costfunc_tuple, vcor.param, jac = numgrad, maxiter=20, disp=True).x
    ex_end, c_end = costfunc(param, v = True, updt = False)
    log.info("end: \nimpurity energy = %20.12f    constraint = %20.12f", ex_end, c_end)
    
    vcor.update(param)
    return vcor, c_begin, c_end


def transformResults(GRhoEmb, E, lattice, basis, ImpHam, H_energy, dmu):
    VA, VB, UA, UB = separate_basis(basis)
    nscsites = basis.shape[-2] / 2
    nbasis = basis.shape[-1]
    R = np.empty((nscsites*2, nbasis*2))
    R[:nscsites, :nbasis] = VA[0]
    R[nscsites:, :nbasis] = UB[0]
    R[:nscsites, nbasis:] = UA[0]
    R[nscsites:, nbasis:] = VB[0]
    GRhoImp = mdot(R, GRhoEmb, R.T)
    occs = np.diag(GRhoImp)
    nelec = np.sum(occs[:nscsites]) - np.sum(occs[nscsites:]) + nscsites
    if E is not None:
        # FIXME energy expression is definitely wrong with mu built in the
        # Hamiltonian
        H1energy, H0energy = H_energy
        rhoA, rhoB, kappaBA = extractRdm(GRhoEmb)

        tempCD, tempCC, tempH0 = transform_imp(basis, lattice, dmu * np.eye(nscsites))

        CDeff = ImpHam.H1["cd"] - H1energy["cd"] - tempCD
        CCeff = ImpHam.H1["cc"] - H1energy["cc"] - tempCC
        H0eff = ImpHam.H0 - H0energy - tempH0
        Efrag = E - np.sum(CDeff[0] * rhoA + CDeff[1] * rhoB) - \
                2 * np.sum(CCeff[0] * kappaBA.T) - H0eff
    else:
        Efrag = None
    return GRhoImp, Efrag, nelec
